//
//  ModalViewController.swift
//  ARKitAppleDemo
//
//  Created by Nathan Chan on 4/26/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissModal))
        view.addGestureRecognizer(gesture)
        
        view.alpha = 0
        view.backgroundColor = .clear
        view.tintColor = .clear
        
        let modalView = UIView()
        modalView.backgroundColor = .clear
        
        view.addSubview(modalView)
        
        modalView.translatesAutoresizingMaskIntoConstraints = false
        modalView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        modalView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        modalView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        modalView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        let animatedImage = UIImage.animatedImage(with: [#imageLiteral(resourceName: "unicornparker3"), #imageLiteral(resourceName: "unicornparker4"), #imageLiteral(resourceName: "unicornparker3"), #imageLiteral(resourceName: "unicornparker4"), #imageLiteral(resourceName: "unicornparker3"), #imageLiteral(resourceName: "unicornparker2"), #imageLiteral(resourceName: "unicornparker1"), #imageLiteral(resourceName: "unicornparker2"), #imageLiteral(resourceName: "unicornparker1"), #imageLiteral(resourceName: "unicornparker2")], duration: 2.5)
        //[#imageLiteral(resourceName: "homeParkerTurns1"), #imageLiteral(resourceName: "homeParkerTurns2"), #imageLiteral(resourceName: "homeParkerTurns3"), #imageLiteral(resourceName: "homeParkerTurns4"), #imageLiteral(resourceName: "homeParkerTurns5"), #imageLiteral(resourceName: "homeParkerTurns6"), #imageLiteral(resourceName: "homeParkerTurns7"), #imageLiteral(resourceName: "homeParkerTurns4"), #imageLiteral(resourceName: "homeParkerTurns3"), #imageLiteral(resourceName: "homeParkerTurns2")]
        let imageView = UIImageView(image: animatedImage)
        
        modalView.addSubview(imageView)
        
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: modalView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: modalView.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: modalView.heightAnchor).isActive = true
    }
    
    @objc func dismissModal() {
        dismiss(animated: true, completion: nil)
    }
}
